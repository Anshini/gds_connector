var ACCESS_TOKEN = "twh3o2W0k1WA2E3V1VGAh0GtgjGz";
var API_KEY = "AIzaSyB_3Udcprm-IijVCkz1Q8Ivixu2P-dOB6M";
var API_BASE_URL = "http://tapclicks-test.apigee.net/";
var DATA_ID = "%id%";
var SERVICE_ID = "services";


function getAuthType() {
  var response = {
    "type": "OAUTH2"
  };
  return response;
}
var getOAuthService = function() {
  return OAuth2.createService('exampleService')
    .setAuthorizationBaseUrl('https://accounts.google.com/o/oauth2/auth')
    .setTokenUrl('https://accounts.google.com/o/oauth2/token')
    .setClientId('%CLIENTID%')
    .setClientSecret('%CLIENTSECRET%')
    .setPropertyStore(PropertiesService.getUserProperties())
    .setCallbackFunction('authCallback')
    .setScope('email');
};
function getConfig(request) {
  var dataViews = getDataViews(API_BASE_URL, SERVICE_ID, DATA_ID, ACCESS_TOKEN);
  var clients = getClients(API_BASE_URL, ACCESS_TOKEN);
  var config = {
  scriptParams: {
    "sampleExtraction": true,
  },
  configParams: [
   {
      "type": "SELECT_SINGLE",
      "name": "dataViews",
      "displayName": "Select Data Views",
      "helpText": "Helper text for select-single",
      "options": dataViews
    },
    {
      "type": "SELECT_MULTIPLE",
      "name": "customerID",
      "displayName": "Select multiple clients or client groups",
      "helpText": "Helper text for select-multiple",
      "options": clients
    },  
    {
      "type": "SELECT_SINGLE",
      "name": "timeframe",
      "displayName": "Select TimeFrame",
      "helpText": "Helper text for select-single",
      "options" : [
            {
                "value": "LAST_30",
                "label": "Last 30 days"
            },
            {
                "value": "LAST_60",
                "label": "Last 60 days"
            },
            {
                "value": "LAST_MONTH",
                "label": "Last month"
            },
            {
                "value": "THIS_MONTH",
                "label": "This month"
            },
            {
                "value": "LAST_WEEK",
                "label": "Last week"
            },
            {
                "value": "THIS_WEEK",
                "label": "This week"
            },
            {
                "value": "YESTERDAY",
                "label": "Yesterday"
            }
        ]
    } 
 ]
 };
 return config;
};
function getSchema(request) {
   var schema = createSchema(API_BASE_URL, SERVICE_ID, DATA_ID, ACCESS_TOKEN, request.configParams.dataViews, request.configParams.customerID, request.configParams.timeframe);
   return {schema: schema};
};

function isAdminUser() {
  return true;
};

function getData(request) {
 var dataSchema = [];
 var schemaProp = PropertiesService.getUserProperties().getProperty('schema');
 var schema = JSON.parse(schemaProp);
 request.fields.forEach(function(field) {
    for (var i = 0; i < schema.length; i++) {
     if (schema[i].name === field.name) {
       dataSchema.push(schema[i]);
       break;
     }
   }
 });
 var url =  PropertiesService.getUserProperties().getProperty('url');
 var headers =
      {
        "Authorization" : "Bearer "+ ACCESS_TOKEN,
      };
 var options =
      {
        "method"  : "GET",
        "headers" : headers,
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
 
 Logger.log("Printing url req: " + url);
 var result = UrlFetchApp.fetch(url, options);
 var columns = JSON.parse(result.getContentText());
 result = columns.data;
 Logger.log("Result is " + JSON.stringify(result));
 sendLogMail();
 var data = [];
 for(var i = 0 ; i < result.length;i++){  
   var values = [];
   for(var j = 0 ; j < dataSchema.length;j++){
     var field = dataSchema[j].name;
     if(result[i] && field in result[i]) {
       var value = result[i][field];
       values.push(value);
     }
   }
   data.push({
     values: values
   });
 };  
 return {
   schema: dataSchema,
   rows: data
 };
};

function sendLogMail() {
 var recipient = Session.getActiveUser().getEmail();
 var subject = 'Oauth - _/';
 var body = Logger.getLog();
 MailApp.sendEmail(recipient, subject, body);
}

function authCallback(request) {
  Logger.log("Inside authCallback Function");
  var authorized = getOAuthService().handleCallback(request);
  if (authorized) {
    return HtmlService.createHtmlOutput('Success! You can close this tab.');
  } else {
    return HtmlService.createHtmlOutput('Denied. You can close this tab');
  };
};

function isAuthValid() {
  Logger.log("Inside isAuthValid Function");
  var service = getOAuthService();
  if (service == null) {
    return false;
  }
  return service.hasAccess();
};

function logRedirectUri() {
  Logger.log("Inside logRedirectUri Function");
  var service = getService();
  Logger.log(service.getRedirectUri());
};

function get3PAuthorizationUrls() {
  Logger.log("Inside get3PAuthorizationUrls function");
  var service = getOAuthService();
  if (service == null) {
    return '';  var apiBaseUrl = getGdsConnectorApiBaseUrl(); 
  return service.getAuthorizationUrl();
}

function resetAuth() {
  var service = getOAuthService()
  service.reset();
}