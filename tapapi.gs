// ******* GET DATA VIEWS - for getConfig function ******* 
function getDataViews(apiBaseUrl, serviceId, dataId, accessToken) { 
  var url = apiBaseUrl +"v2/" + serviceId + "/" + dataId + "/data/data_views";
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
 var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
  try{
   var result = UrlFetchApp.fetch(url, options);
   if (result.getResponseCode() == 200) {
        var params = JSON.parse(result.getContentText());  
        var data = params.data;
        var dataViews = [];
        for(i in data){
         var obj = { 
           "label": data[i].name,
           "value": data[i].id
         };
       dataViews.push(obj);
      }
      return dataViews;
  }
  }catch(e){
  Logger.log("Error occcured in accessing data, possible reason : "+ e);
  }
};

// ******* GET CLIENTS - for getConfig function ******* 
function getClients(apiBaseUrl, accessToken){ 
  var url = apiBaseUrl +"v2/clients";
   var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
   var result = UrlFetchApp.fetch(url, options);
     if (result.getResponseCode() == 200) {
        var params = JSON.parse(result.getContentText());  
        var data = params.data;
        var clients = [];
        for(i in data){
        var obj = { 
           "label": data[i].company_name,
           "value": data[i].id
        };
       clients.push(obj);
     }
      return clients;
  }
};

function createSchema(apiBaseUrl, serviceId, dataId, accessToken, dataViews, customerID, timeframe){
  var fields ="";
  var dataSchema = [];
  var url = apiBaseUrl +"v2/" + serviceId + "/" + dataId + "/data/" + dataViews + "?metadata=1";
  var headers =
      {
        "Authorization" : "Bearer "+ accessToken,
      };
  var options =
      {
        "method"  : "GET",
        "headers" : headers,   
        "followRedirects" : true,
        "muteHttpExceptions": true
      };
   var result = UrlFetchApp.fetch(url, options);
   var columns = JSON.parse(result.getContentText());  
   columns = columns.metadata.columns;
   for(var value in columns){
       fields+=columns[value].field;
       if (value != columns.length - 1) {
          fields += ","
        }       
       var dataType;
       switch (columns[value].format) {
           case "integer": 
                 dataType = "NUMBER";
                 break;
           case "decimal":
                 dataType = "NUMBER";
                 break;
           case "percent":
                 dataType = "NUMBER";
                 break;
           case "currency":
                 dataType = "NUMBER";
                 break;
           case "string":
                 dataType = "STRING";
                 break;
           case "link":
                 dataType = "STRING";
                 break;
           case "datetime":
                 dataType = "STRING";
                 break;
           case "time":
                 dataType = "STRING";
                 break;
           case "bool":
                 dataType = "BOOLEAN";
                 break;
           case "id":
                 dataType = "STRING";
                 break;
           case "image":
                 dataType = "STRING";
                 break;
           default :
                 dataType = "STRING";
                 break;
     };
      
      var conceptType;
        if (columns[value].is_metric) {
             conceptType = "METRIC";
             } else {
             conceptType = "DIMENSION";
             };  
      dataSchema[value] = {
             "name": columns[value].field,
             "label": columns[value].label,
             "dataType": dataType,
             "semantics": {
                   "conceptType": conceptType
                 }
          };    
   }
   PropertiesService.getUserProperties().setProperty('schema', JSON.stringify(dataSchema));
   var date = dateRange(timeframe);
   url = apiBaseUrl +"v2/" + serviceId + "/" + dataId + "/data/" + dataViews + "?daterange=" + date + "&fields=" + fields + "&customer_id="+ customerID + "&timegrouping=hourly";
   PropertiesService.getUserProperties().setProperty('url', url); 
   return dataSchema;
};

function dateRange(timeframe){
        var startDate = "";
        var endDate = "";
        var today = new Date();
            switch (timeframe) {
         case "LAST_30":
            startDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
            endDate = today;
            break;
         case "LAST_60":
              startDate = new Date(today.getTime() - (60 * 24 * 60 * 60 * 1000));
              endDate = today;
              break;
         case "LAST_MONTH":
              var startDate = new Date(today.getTime() - (60 * 24 * 60 * 60 * 1000));
              var endDate = new Date(today.getTime() - (30 * 24 * 60 * 60 * 1000));
              break;
         case "THIS_MONTH":
              var days = today.getDate();
              var startDate = new Date(today.getTime() - (days * 24 * 60 * 60 * 1000));
              var endDate = today;
              break;
         case "LAST_WEEK":
              var startDate = new Date(today.getTime() - (14 * 24 * 60 * 60 * 1000));
              var endDate = new Date(today.getTime() - (7 * 24 * 60 * 60 * 1000));
              break;
         case "THIS_WEEK":
              var startDate = new Date(today.getTime() - (7 * 24 * 60 * 60 * 1000));
              var endDate = today;
              break;
          case "YESTERDAY":
              var startDate = new Date(today.getTime() - (2 * 24 * 60 * 60 * 1000));
              var endDate = startDate;
              break;
    }
   var daterange = startDate.toISOString().slice(0,10) + "%7C" + endDate.toISOString().slice(0,10);
   return daterange;
};